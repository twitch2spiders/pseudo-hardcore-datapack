# Pseudo-Hardcore Datapack

This is a datapack for minecraft that will prevent player respawning and will require new bodies to be crafted for players.

# Installation

The easiest way to put this datapack on a world is to go into the datapacks folder and clone this repository.

```bash
cd ~/AppData/Roaming/.minecraft/saves/YOUR_WORLD/datapacks
git clone git@gitlab.com:twitch2spiders/pseudo-hardcore-datapack.git
```

Alternatively, you can download this repository as a zip file and place it into your datapacks folder.

Open your minecraft world, open the menu, press `Options/Resource Packs/Open Pack Folder`.

In the foler that opens, travel up one directory to the .minecraft folder then navigate to the `saves/YOUR_WORLD/datapacks`.

Download this repository as a zip file, place it inside and unzip the file.

# Usage

This pack will force survival gamemode, to escape this, simply give a player the admin tag.

```
/tag TARGET add admin
```

Each player will recieve a journal with instructions on the specifics