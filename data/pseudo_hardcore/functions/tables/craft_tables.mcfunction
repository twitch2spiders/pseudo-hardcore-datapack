# Table Eat
execute as @e[tag=Table_Center] at @s if entity @e[type=item,nbt={Item:{id:"minecraft:rotten_flesh",Count:1b}},distance=..0.3] run scoreboard players add @s charge 1
execute as @e[tag=Table_Center] at @s if entity @e[type=item,nbt={Item:{id:"minecraft:rotten_flesh",Count:1b}},distance=..0.3] as @e[type=item,nbt={Item:{id:"minecraft:rotten_flesh",Count:1b}},distance=..0.3] at @s run particle crimson_spore ~ ~ ~ 0 0 0 0 1
execute as @e[tag=Table_Center] at @s if entity @e[type=item,nbt={Item:{id:"minecraft:rotten_flesh",Count:1b}},distance=..0.3] as @e[type=item,nbt={Item:{id:"minecraft:rotten_flesh",Count:1b}},distance=..0.3] at @s run playsound minecraft:entity.generic.extinguish_fire ambient @a ~ ~ ~ 0.2 2
execute as @e[tag=Table_Center] at @s if entity @e[type=item,nbt={Item:{id:"minecraft:rotten_flesh",Count:1b}},distance=..0.3] run kill @e[type=item,nbt={Item:{id:"minecraft:rotten_flesh"}},distance=..0.3,limit=1]

execute as @e[tag=Table_Center] at @s if entity @e[type=item,nbt={Item:{id:"minecraft:spider_eye",Count:1b}},distance=..0.3] run summon experience_orb ~ ~0.3 ~ {Value:3}
execute as @e[tag=Table_Center] at @s if entity @e[type=item,nbt={Item:{id:"minecraft:spider_eye",Count:1b}},distance=..0.3] as @e[type=item,nbt={Item:{id:"minecraft:spider_eye",Count:1b}},distance=..0.3] at @s run particle bubble ~ ~ ~ 0 0 0 0 1
execute as @e[tag=Table_Center] at @s if entity @e[type=item,nbt={Item:{id:"minecraft:spider_eye",Count:1b}},distance=..0.3] as @e[type=item,nbt={Item:{id:"minecraft:spider_eye",Count:1b}},distance=..0.3] at @s run playsound minecraft:block.bubble_column.bubble_pop ambient @a ~ ~ ~ 1.2 2
execute as @e[tag=Table_Center] at @s if entity @e[type=item,nbt={Item:{id:"minecraft:spider_eye",Count:1b}},distance=..0.3] run kill @e[type=item,nbt={Item:{id:"minecraft:spider_eye"}},distance=..0.3,limit=1]

execute as @e[tag=Table_Center] at @s if entity @e[type=item,nbt={Item:{id:"minecraft:bone",Count:1b}},distance=..0.3] run summon experience_orb ~ ~0.3 ~ {Value:2}
execute as @e[tag=Table_Center] at @s if entity @e[type=item,nbt={Item:{id:"minecraft:bone",Count:1b}},distance=..0.3] as @e[type=item,nbt={Item:{id:"minecraft:bone",Count:1b}},distance=..0.3] at @s run particle bubble ~ ~ ~ 0 0 0 0 1
execute as @e[tag=Table_Center] at @s if entity @e[type=item,nbt={Item:{id:"minecraft:bone",Count:1b}},distance=..0.3] as @e[type=item,nbt={Item:{id:"minecraft:bone",Count:1b}},distance=..0.3] at @s run playsound minecraft:block.bubble_column.bubble_pop ambient @a ~ ~ ~ 1.2 2
execute as @e[tag=Table_Center] at @s if entity @e[type=item,nbt={Item:{id:"minecraft:bone",Count:1b}},distance=..0.3] run kill @e[type=item,nbt={Item:{id:"minecraft:bone"}},distance=..0.3,limit=1]

execute as @e[tag=Table_Center] at @s if entity @e[type=item,nbt={Item:{id:"minecraft:gunpowder",Count:1b}},distance=..0.3] run summon experience_orb ~ ~0.3 ~ {Value:16}
execute as @e[tag=Table_Center] at @s if entity @e[type=item,nbt={Item:{id:"minecraft:gunpowder",Count:1b}},distance=..0.3] as @e[type=item,nbt={Item:{id:"minecraft:gunpowder",Count:1b}},distance=..0.3] at @s run particle bubble ~ ~ ~ 0 0 0 0 1
execute as @e[tag=Table_Center] at @s if entity @e[type=item,nbt={Item:{id:"minecraft:gunpowder",Count:1b}},distance=..0.3] as @e[type=item,nbt={Item:{id:"minecraft:gunpowder",Count:1b}},distance=..0.3] at @s run playsound minecraft:block.bubble_column.bubble_pop ambient @a ~ ~ ~ 1.2 2
execute as @e[tag=Table_Center] at @s if entity @e[type=item,nbt={Item:{id:"minecraft:gunpowder",Count:1b}},distance=..0.3] run kill @e[type=item,nbt={Item:{id:"minecraft:gunpowder"}},distance=..0.3,limit=1]

execute as @e[tag=Table_Center] at @s if entity @e[type=item,nbt={Item:{id:"minecraft:gold_nugget",Count:1b}},distance=..0.3] at @e[type=item,nbt={Item:{id:"minecraft:gold_nugget",Count:1b}},distance=..0.3,sort=nearest] run summon item ~ ~ ~ {Item:{id:"minecraft:iron_nugget",Count:1b}}
execute as @e[tag=Table_Center] at @s if entity @e[type=item,nbt={Item:{id:"minecraft:gold_nugget",Count:1b}},distance=..0.3] as @e[type=item,nbt={Item:{id:"minecraft:gold_nugget",Count:1b}},distance=..0.3] at @s run particle flame ~ ~ ~ 0 0 0 0 1
execute as @e[tag=Table_Center] at @s if entity @e[type=item,nbt={Item:{id:"minecraft:gold_nugget",Count:1b}},distance=..0.3] as @e[type=item,nbt={Item:{id:"minecraft:gold_nugget",Count:1b}},distance=..0.3] at @s run playsound minecraft:entity.generic.extinguish_fire ambient @a ~ ~ ~ 1.2 2
execute as @e[tag=Table_Center] at @s if entity @e[type=item,nbt={Item:{id:"minecraft:gold_nugget",Count:1b}},distance=..0.3] run kill @e[type=item,nbt={Item:{id:"minecraft:gold_nugget"}},distance=..0.3,limit=1]

execute as @e[tag=Table_Center] at @s if entity @e[type=item,nbt={Item:{id:"minecraft:poisonous_potato",Count:1b}},distance=..0.3] at @e[type=item,nbt={Item:{id:"minecraft:poisonous_potato",Count:1b}},distance=..0.3,sort=nearest] run summon item ~ ~ ~ {Item:{id:"minecraft:potato",Count:1b}}
execute as @e[tag=Table_Center] at @s if entity @e[type=item,nbt={Item:{id:"minecraft:poisonous_potato",Count:1b}},distance=..0.3] as @e[type=item,nbt={Item:{id:"minecraft:poisonous_potato",Count:1b}},distance=..0.3] at @s run particle flame ~ ~ ~ 0 0 0 0 1
execute as @e[tag=Table_Center] at @s if entity @e[type=item,nbt={Item:{id:"minecraft:poisonous_potato",Count:1b}},distance=..0.3] as @e[type=item,nbt={Item:{id:"minecraft:poisonous_potato",Count:1b}},distance=..0.3] at @s run playsound minecraft:block.chorus_flower.grow ambient @a ~ ~ ~ 1.2 2
execute as @e[tag=Table_Center] at @s if entity @e[type=item,nbt={Item:{id:"minecraft:poisonous_potato",Count:1b}},distance=..0.3] run kill @e[type=item,nbt={Item:{id:"minecraft:poisonous_potato"}},distance=..0.3,limit=1]

execute as @e[tag=Table_Center] at @s if entity @e[type=item,nbt={Item:{id:"minecraft:enchanted_golden_apple",Count:1b}},distance=..0.3] at @e[type=item,nbt={Item:{id:"minecraft:enchanted_golden_apple",Count:1b}},distance=..0.3,sort=nearest] run summon item ~ ~ ~ {Item:{id:"minecraft:totem_of_undying",Count:1b}}
execute as @e[tag=Table_Center] at @s if entity @e[type=item,nbt={Item:{id:"minecraft:enchanted_golden_apple",Count:1b}},distance=..0.3] as @e[type=item,nbt={Item:{id:"minecraft:enchanted_golden_apple",Count:1b}},distance=..0.3] at @s run particle angry_villager ~ ~ ~ 0 0 0 0 10
execute as @e[tag=Table_Center] at @s if entity @e[type=item,nbt={Item:{id:"minecraft:enchanted_golden_apple",Count:1b}},distance=..0.3] as @e[type=item,nbt={Item:{id:"minecraft:enchanted_golden_apple",Count:1b}},distance=..0.3] at @s run playsound minecraft:block.glass.break ambient @a ~ ~ ~ 1.2 1
execute as @e[tag=Table_Center] at @s if entity @e[type=item,nbt={Item:{id:"minecraft:enchanted_golden_apple",Count:1b}},distance=..0.3] run kill @e[type=item,nbt={Item:{id:"minecraft:enchanted_golden_apple"}},distance=..0.3,limit=1]

# Table Craft
execute as @e[tag=Table_Center,scores={charge=64..}] at @s run particle smoke ~ ~0.5 ~ 0.2 0.2 0.2 0 100
execute as @e[tag=Table_Center,scores={charge=64..}] at @s run playsound minecraft:entity.zombie_villager.cure ambient @a ~ ~ ~ 0.3 2
execute as @e[tag=Table_Center,scores={charge=64..}] at @s run summon item ~ ~ ~ {Item:{id:"minecraft:zombie_spawn_egg",Count:1b,tag:{display:{Name:'{"text":"Empty Husk","color":"dark_red","bold":true,"italic":true}',Lore:['{"text":"A vessel void of soul","color":"dark_purple"}']},EntityTag:{id:"minecraft:zombie",Silent:1b,Invulnerable:1b,CustomNameVisible:1b,NoAI:1b,CanPickUpLoot:0b,CustomName:'{"text":"Empty Husk","color":"dark_red","bold":true,"italic":true}',ArmorItems:[{id:"minecraft:leather_boots",Count:1b},{id:"minecraft:leather_leggings",Count:1b},{id:"minecraft:leather_chestplate",Count:1b},{}]}}}}
execute as @e[tag=Table_Center,scores={charge=64..}] at @s run scoreboard players remove @s charge 64