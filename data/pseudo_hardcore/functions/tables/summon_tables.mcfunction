execute as @s at @s run playsound minecraft:block.beacon.power_select ambient @a ~ ~ ~ 0.8 1.2
execute as @s at @s run particle smoke ~ ~0.3 ~ 0.2 0.5 0.2 0 100

execute as @s at @s run summon armor_stand ~ ~-0.2 ~ {Tags:["Custom_Crafting_Table","Table_Rotate"],Marker:1,NoGravity:1b,Invulnerable:1b,Invisible:1b,Small:1b,HandItems:[{id:"end_rod",Count:1b},{id:"end_rod",Count:1b}],Pose:{LeftArm:[0f,90f,225f],RightArm:[0f,270f,135f]}}
execute as @s at @s run summon armor_stand ~ ~-0.2 ~ {Tags:["Custom_Crafting_Table","Table_Rotate"],Marker:1,NoGravity:1b,Invulnerable:1b,Invisible:1b,Small:1b,HandItems:[{id:"end_rod",Count:1b},{id:"end_rod",Count:1b}],Rotation:[90f],Pose:{LeftArm:[0f,90f,225f],RightArm:[0f,270f,135f]}}
execute as @s at @s run summon armor_stand ~ ~-0.2 ~ {Tags:["Custom_Crafting_Table","Table_Center"],Marker:1,NoGravity:1b,Invulnerable:1b,Invisible:1b,Small:1b,ArmorItems:[{},{},{},{id:"gilded_blackstone",Count:1b}]}
execute as @s at @s run summon armor_stand ~ ~.22 ~ {Tags:["Custom_Crafting_Table","Table_Hat"],Marker:1,NoGravity:1b,Invulnerable:1b,Invisible:1b,Small:1b,ArmorItems:[{},{},{},{id:"light_weighted_pressure_plate",Count:1b}]}
execute as @s at @s run summon armor_stand ~ ~ ~ {Tags:["Custom_Crafting_Table","Table_Weight"],NoGravity:1b,Invulnerable:1b,Invisible:1b,Small:1b}
execute as @s at @s run setblock ~ ~ ~ polished_blackstone_pressure_plate[powered=true]

execute as @s[tag=Spawn_Table] at @s run kill @e[type=item_frame,limit=1,sort=nearest,distance=..0.5,nbt={Item:{id:"minecraft:ender_eye",Count:1b}}]
execute as @s[tag=Spawn_Table] at @s run kill @e[type=item,limit=1,sort=nearest,nbt={Item:{id:"minecraft:gunpowder"}},distance=..0.5]
execute as @s[tag=Spawn_Table] at @s run kill @e[type=item,limit=1,sort=nearest,nbt={Item:{id:"minecraft:gold_ingot"}},distance=..0.5]
execute as @s[tag=Spawn_Table] at @s run kill @e[type=item,limit=1,sort=nearest,nbt={Item:{id:"minecraft:emerald"}},distance=..0.5]
execute as @s[tag=Spawn_Table] at @s run kill @s