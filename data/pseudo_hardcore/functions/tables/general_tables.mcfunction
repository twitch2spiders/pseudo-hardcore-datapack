# Spawn Table
execute as @e[type=item_frame,nbt={Facing:1b,Item:{id:"minecraft:ender_eye",Count:1b}}] at @s if entity @e[type=item,nbt={Item:{id:"minecraft:gunpowder"}},distance=..0.5] if entity @e[type=item,nbt={Item:{id:"minecraft:gold_ingot"}},distance=..0.5] if entity @e[type=item,nbt={Item:{id:"minecraft:emerald"}},distance=..0.5] run summon armor_stand ~ ~ ~ {Tags:["Spawn_Table"],Invisible:1b,Invulnerable:1b}
execute as @e[tag=Spawn_Table] at @s run function pseudo_hardcore:tables/summon_tables
