# Handle Tables
function pseudo_hardcore:tables/animate_tables
function pseudo_hardcore:tables/kill_tables
function pseudo_hardcore:tables/general_tables
function pseudo_hardcore:tables/craft_tables

# Handle Deaths
execute as @a[tag=!dead,gamemode=!survival] unless entity @s[tag=admin,gamemode=creative] run gamemode survival
execute as @a[tag=dead,gamemode=!spectator] run gamemode spectator @s
execute as @a[scores={hasDied=1..}] run tag @s add dead
execute as @a[tag=dead,scores={hasDied=1..}] run scoreboard players set @s hasDied 0

# Handle Husks
tag @e[name="Empty Husk",type=zombie] add husk

execute as @a[tag=dead,gamemode=spectator] at @s if entity @e[name="Empty Husk",distance=0..0.01] run function pseudo_hardcore:revive

# Give Book
give @a[tag=!hasBook] written_book{pages:['["",{"text":"Tricking the Reaper","bold":true,"underlined":true,"color":"dark_blue"},{"text":"\\nDeath doesn\'t have to be permanent! For anyone wishing to escape the looming threat of permanent death, there are a few steps one can take to prevent thier soul from passing!","color":"reset"}]','["",{"text":"Crafting the ","bold":true,"underlined":true,"color":"dark_blue"},{"text":"Altar","bold":true,"underlined":true,"obfuscated":true,"color":"dark_blue"},{"text":"\\nIn order to prepare a new vessel for your soul, you will need a special altar. Gather the following materials:\\n-Item Frame\\n-Ender Eye\\n-Gold Ingot\\n-Emerald\\n-Gunpowder","color":"reset"}]','{"text":"Placing the item frame on the floor where you wish the altar to be placed, insert the ender eye then throw the rest of the ingredients on top! Voila, you now have a spectral altar you can use to prepare for your rebirth!"}','["",{"text":"A New Home","bold":true,"underlined":true,"color":"dark_blue"},{"text":"\\nWhen your body happens to expire, you will need a new vessel to inhabit. Simply feed flesh of the fallen to your altar to begin building your new body. When you have fed the altar enough, it will reward you with exactly what you need!","color":"reset"}]','["",{"text":"Untold Mystery...","bold":true,"underlined":true,"color":"dark_blue"},{"text":"\\nUnsure what good the altar is now that you have your next body? Well the altar serves a few other purposes. Try feeding the altar some of the materials on the next page!","color":"reset"}]','{"text":"-Enchanted golden apple\\n-Poisonous potato\\n-Gold nugget\\n-Gunpowder\\n-Bone\\n-Spider eye"}'],title:"Hitchhiker's Guide to Death",author:"An Experienced Hiker",generation:3}
tag @a[tag=!hasBook] add hasBook