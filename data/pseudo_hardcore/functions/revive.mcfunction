execute as @a[tag=dead,gamemode=spectator] at @s as @e[name="Empty Husk",distance=0..1] run tag @s add used
execute as @e[name="Empty Husk",tag=used] at @s run particle minecraft:smoke ~ ~ ~ 1 1 1 0.1 100 normal
execute as @e[name="Empty Husk",tag=used] at @s as @p[tag=dead] run tag @s remove dead
kill @e[name="Empty Husk",tag=used]
say A player has respawned